import psycopg2

class Conexion_db:
    def __init__(self, mi_host, database, user, passwd):
        try:
            self.conn=psycopg2.connect(    
                host = mi_host,
                database = database,
                user = user,
                password = passwd)
            self.cur = self.conn.cursor()
        except:
            print("Error en la conexión")

    def consultar_db(self, query):
        try:
            self.cur.execute(query)
            response=self.cur.fetchall() #para leer retorno de la query
            return response
        
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)
            return "error"


    def escribir_db(self, query):
        try:
            self.cur.execute(query)
            self.conn.commit()
        
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)


    def cerrar_db(self):
        self.cur.close()
        self.conn.close()
